#-*- coding:utf-8 -*-

import os
import sys
 
os.environ['DJANGO_SETTINGS_MODULE'] = 'bae_django.settings'
 
path = os.path.dirname(os.path.abspath(__file__)) + '/bae_django'
if path not in sys.path:
    sys.path.insert(1, path)
deps_path = os.path.join(os.path.split(os.path.realpath(__file__))[0],'deps')
if deps_path not in sys.path:
    sys.path.insert(0, deps_path)

from django.core.handlers.wsgi import WSGIHandler
from bae.core.wsgi import WSGIApplication
 
application = WSGIApplication(WSGIHandler())